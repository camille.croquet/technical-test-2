# Wizbii Wallet (Test technique)

## Description

L'objectif de ce test technique est de reproduire le process que nous suivons dans la quasi totalité de nos développements :

- Récupérer les sources d'une application existante
- Comprendre le code
- Comprendre les besoins du PO
- Arriver à le faire fonctionner
- Le faire évoluer pour prendre en compte les besoins exprimés par le Product Owner
- Envoyer une Merge Request à l'équipe expliquant ce qui a été réalisé

Cette application de test est simple : elle permet de gérer des porte-monnaie électroniques en faisant des virements d'un porte-monnaie (Wallet) à un autre. Le projet a été initialisé avec **Yarn**, **Angular 11**, **SCSS**, **Jest** et **Angular Material**. Dans le cadre du test, plusieurs choix sont imposés dans ce code :

- l'utilisation d'un State Manager pour la gestion de la liste des transactions, ici [NgXs](https://www.ngxs.io/)

L'objectif de ce test est de nous montrer comment vous travaillez à l'heure actuelle ou comment vous souhaitez travailler. Il existe en effet plusieurs possibilités pour répondre aux besoins exprimés ci-dessous. Choisissez celle qui vous correspond le mieux, expliquez-la dans votre MR et venez la défendre devant d'autres développeurs lors d'un entretien technique :)

Le projet étant outillé avec Jest ([Documentation de Jest](https://jestjs.io/docs/en/expect)), vous avez la possibilité d'écrire des tests unitaires. Nos règles de linting ont été implémentées dans ce projet. La CI de Gitlab vous permet de vérifier que ces implémentations ont été respectées.

Vous trouverez ci-dessous différentes stories écrites par le Product Owner du projet, de la plus prioritaire à la moins prioritaire. Nous vous conseillons de passer 2 fois 2 heures sur le travail qui vous est proposé.

## Documentation de l'API

Les développeurs Backend vous fournissent une documentation de l'API que vous allez trouver ici : [Documentation de la Wallet Management API](https://wallet.api.wizbii.com/api/doc).

Toutes les requêtes nécessitent d'être authentifié. Pour cela vous devrez fournir votre access token (que vous pouvez retrouver en [bas de page](#variables-personnelles-pour-le-test)) via le header `X-Access-Token`.

**NB** : Les transactions ne débitent pas d'argent sur le wallet courant afin de ne pas vous bloquer pendant ce test.

## Besoins du PO

Le Product Owner a exprimé plusieurs besoins :

### Pouvoir afficher l'id du wallet courant

En tant qu'utilisateur, je souhaite pouvoir afficher l'id du wallet dans le header en desktop et en mobile afin de voir que je suis bien connecté.

| Desktop                                                                                      | Mobile                                                                                                       |
| -------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------ |
| ![Header](https://storage.googleapis.com/wizbii/static-assets-v4/test-tech-front/Header.png) | ![Header mobile](https://storage.googleapis.com/wizbii/static-assets-v4/test-tech-front/Header%20Mobile.png) |

### Afficher toutes les transactions

En tant qu'utilisateur, je souhaite afficher la liste des transactions sur Desktop et Mobile afin d'avoir une vue d'ensemble de mon compte.

Route: `/transactions`

|                                                             Desktop                                                              |                                                                      Mobile                                                                      |
| :------------------------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------: |
| ![Liste des transactions](https://storage.googleapis.com/wizbii/static-assets-v4/test-tech-front/Liste%20des%20Transactions.png) | ![Liste des transactions mobile](https://storage.googleapis.com/wizbii/static-assets-v4/test-tech-front/Liste%20des%20transactions%20Mobile.png) |

### Afficher le détail d'une transaction

En tant qu'utilisateur, je souhaite être redirigé vers une transaction lorsque je clique sur celle-ci dans la liste afin d'avoir les détails d'une transaction.

Route: `/transaction/123456789`

![Détail d'une transaction](https://storage.googleapis.com/wizbii/static-assets-v4/test-tech-front/De%CC%81tail%20d%27une%20transaction.png)

### Pouvoir envoyer de l'argent

En tant qu'utilisateur, je souhaite pouvoir envoyer de l'argent à un autre utilisateur afin de le rembourser.

![Envoyer de l'argent](https://storage.googleapis.com/wizbii/static-assets-v4/test-tech-front/Ajout%20d%27une%20transaction.png)

### Pouvoir afficher mon solde

En tant qu'utilisateur, je souhaite pouvoir afficher le solde de mon compte au hover de l'id du wallet afin de voir s'il me reste de l'argent sur mon compte.

![enter image description here](https://storage.googleapis.com/wizbii/static-assets-v4/test-tech-front/Affichage%20du%20solde.png)

## Variables personnelles pour le test

Pour ce test, voici les variables qui vous sont propres :

> access token : `301c798ecf2f43a424c46d826a994d20`  
> wallet ID : `camille-croquet`

