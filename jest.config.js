const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { compilerOptions } = require('./src/tsconfig.frontend');

module.exports = {
  name: 'wallet-management-front',
  preset: 'jest-preset-angular',
  roots: ['<rootDir>/src/'],
  testMatch: ['**/+(*.)+(spec|test).+(ts|js)?(x)'],
  setupFilesAfterEnv: ['<rootDir>/src/test.ts'],
  coverageReporters: ['lcov', 'json', 'text', 'clover'],
  coverageDirectory: 'dist/tests',
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths || {}, { prefix: '<rootDir>/src/' }),
  reporters: [
    'default',
    [
      'jest-junit',
      { suiteName: 'WalletManagementFront Tests', outputDirectory: './dist/tests/', outputName: 'junit.xml' },
    ],
  ],
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/src/tsconfig.spec.json',
    },
  },
  transform: {
    '^.+\\.js$': 'babel-jest',
    '^.+\\.(ts|html)$': 'ts-jest',
  },
};
