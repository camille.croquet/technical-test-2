import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, NgModule, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { Wallet } from '@models/wallet.model';
import { WalletService } from '@webservices/wallet/wallet.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-wallet-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
  walletId: string;
  wallet$: Observable<Wallet>;

  constructor(private readonly walletService: WalletService) {
    this.walletId = localStorage.getItem('walletId');
  }

  ngOnInit() {
    this.wallet$ = this.walletService.checkWalletValidity(this.walletId);
  }
}

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, MatCardModule, MatIconModule, MatToolbarModule, MatTooltipModule],
  exports: [HeaderComponent],
})
export class HeaderModule {}
