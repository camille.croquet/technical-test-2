import { CommonModule } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Inject, NgModule } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Transaction } from '@models/transaction.model';
import { WalletService } from '@webservices/wallet/wallet.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  templateUrl: './transaction-modal.component.html',
  styleUrls: ['./transaction-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionModalComponent {
  transactionForm = new FormGroup({
    receiver: new FormControl(undefined, Validators.required),
    amount: new FormControl(0, [Validators.required, Validators.min(1)]),
    comment: new FormControl(),
  });

  get receiver(): FormControl {
    return this.transactionForm?.get('receiver') as FormControl;
  }

  get amount(): FormControl {
    return this.transactionForm?.get('amount') as FormControl;
  }

  get comment(): FormControl {
    return this.transactionForm?.get('comment') as FormControl;
  }

  transaction: Partial<Transaction>;

  errorMessage$ = new BehaviorSubject<string | undefined>(undefined);

  errors: Record<number, string> = {
    400: 'Fonds insuffisants',
    404: "Le wallet n'existe pas",
  };

  constructor(
    public dialogRef: MatDialogRef<TransactionModalComponent>,
    private readonly walletService: WalletService,
    @Inject(MAT_DIALOG_DATA) public data: { walletId: string }
  ) {}

  submitForm(form: FormGroup): void {
    if (!form.valid && form.invalid) {
      return;
    }

    this.transaction = {
      senderWalletId: this.data.walletId,
      receiverWalletId: this.receiver.value,
      comment: this.comment.value,
      amount: this.amount.value,
    };

    this.walletService.postTransaction(this.transaction).subscribe({
      next: () => {
        this.dialogRef.close(true);
      },
      error: (err: HttpErrorResponse) => {
        this.errorMessage$.next(this.errors[err.status]);
      },
    });
  }
}

@NgModule({
  declarations: [TransactionModalComponent],
  imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatButtonModule],
  exports: [TransactionModalComponent],
})
export class TransactionsModule {}
