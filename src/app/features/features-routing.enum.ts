export enum FeaturesRoutingEnum {
  SignIn = 'sign-in',
  TransactionDetails = 'transactions/:id',
  Transactions = 'transactions',
}
