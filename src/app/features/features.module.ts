import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeaturesRoutingEnum } from '@features/features-routing.enum';

const routes: Routes = [
  { path: '', redirectTo: `/${FeaturesRoutingEnum.SignIn}`, pathMatch: 'full' },
  {
    path: FeaturesRoutingEnum.SignIn,
    loadChildren: () => import('./sign-in/sign-in.module').then((m) => m.SignInModule),
  },
  {
    path: FeaturesRoutingEnum.TransactionDetails,
    loadChildren: () =>
      import('./transaction-details/transaction-details.module').then((m) => m.TransactionDetailsModule),
  },
  {
    path: FeaturesRoutingEnum.Transactions,
    loadChildren: () => import('./transactions/transactions.module').then((m) => m.TransactionsModule),
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
})
export class FeaturesModule {}
