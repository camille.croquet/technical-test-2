import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SignInComponent } from '@features/sign-in/sign-in/sign-in.component';
import { WalletService } from '@webservices/wallet/wallet.service';
import { MockModule } from 'ng-mocks';
import { of, throwError } from 'rxjs';

describe('SignInComponent', () => {
  let component: SignInComponent;
  let fixture: ComponentFixture<SignInComponent>;
  let walletService: WalletService;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        MockModule(MatFormFieldModule),
        MockModule(MatInputModule),
        MockModule(MatCardModule),
      ],
      declarations: [SignInComponent],
      providers: [{ provide: WalletService, useValue: { checkWalletValidity: jest.fn() } }],
    }).compileComponents();

    walletService = TestBed.inject(WalletService);
    router = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect if wallet id is valid', () => {
    walletService.checkWalletValidity = jest.fn().mockReturnValue(of(true));
    router.navigate = jest.fn();
    component.walletId.setValue('WALLET_ID');

    component.submitForm(component.signInForm);

    expect(walletService.checkWalletValidity).toHaveBeenCalledWith('WALLET_ID');
    expect(router.navigate).toHaveBeenCalledWith(['', 'transactions']);
  });

  it('should set error mesage if wallet id is not valid', () => {
    walletService.checkWalletValidity = jest.fn().mockReturnValue(throwError({ status: 404 }));
    router.navigate = jest.fn();
    component.walletId.setValue('WALLET_ID');

    component.submitForm(component.signInForm);

    expect(walletService.checkWalletValidity).toHaveBeenCalledWith('WALLET_ID');
    expect(router.navigate).not.toHaveBeenCalled();
    expect(component.errorMessage$.value).toEqual("Le wallet n'existe pas");
  });

  it('should set error mesage if not authorized', () => {
    walletService.checkWalletValidity = jest.fn().mockReturnValue(throwError({ status: 403 }));
    router.navigate = jest.fn();
    component.walletId.setValue('WALLET_ID');

    component.submitForm(component.signInForm);

    expect(walletService.checkWalletValidity).toHaveBeenCalledWith('WALLET_ID');
    expect(router.navigate).not.toHaveBeenCalled();
    expect(component.errorMessage$.value).toEqual('Accès refusé');
  });

  it('should set error mesage if not logged', () => {
    walletService.checkWalletValidity = jest.fn().mockReturnValue(throwError({ status: 401 }));
    router.navigate = jest.fn();
    component.walletId.setValue('WALLET_ID');

    component.submitForm(component.signInForm);

    expect(walletService.checkWalletValidity).toHaveBeenCalledWith('WALLET_ID');
    expect(router.navigate).not.toHaveBeenCalled();
    expect(component.errorMessage$.value).toEqual("Vous n'êtes pas authentifié");
  });

  it('should do nothing if value is empty', () => {
    walletService.checkWalletValidity = jest.fn().mockReturnValue(of(true));
    router.navigate = jest.fn();

    component.submitForm(component.signInForm);

    expect(walletService.checkWalletValidity).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();
  });
});
