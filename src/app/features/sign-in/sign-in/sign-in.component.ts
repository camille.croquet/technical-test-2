import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FeaturesRoutingEnum } from '@features/features-routing.enum';
import { Wallet } from '@models/wallet.model';
import { WalletService } from '@webservices/wallet/wallet.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignInComponent implements OnDestroy {
  signInForm = new FormGroup({
    walletId: new FormControl(undefined, Validators.required),
  });

  errorMessage$ = new BehaviorSubject<string | undefined>(undefined);

  get walletId(): FormControl {
    return this.signInForm?.get('walletId') as FormControl;
  }

  errors: Record<number, string> = {
    401: "Vous n'êtes pas authentifié",
    403: 'Accès refusé',
    404: "Le wallet n'existe pas",
  };

  wallet: Wallet;

  constructor(private readonly walletService: WalletService, private readonly router: Router) {}

  submitForm(form: FormGroup): void {
    if (!form.valid && form.invalid) {
      return;
    }

    this.walletService.checkWalletValidity(this.walletId.value).subscribe({
      next: (wallet) => {
        this.wallet = wallet;
        localStorage.setItem('walletId', this.wallet._id);
        this.router.navigate(['', FeaturesRoutingEnum.Transactions]);
      },
      error: (err: HttpErrorResponse) => {
        this.errorMessage$.next(this.errors[err.status]);
      },
    });
  }

  ngOnDestroy() {
    this.errorMessage$.complete();
  }
}
