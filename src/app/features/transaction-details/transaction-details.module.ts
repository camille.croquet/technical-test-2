import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { TransactionDetailsComponent } from '@features/transaction-details/transaction-details/transaction-details.component';
import { NgxsModule } from '@ngxs/store';

const routes: Routes = [{ path: '', component: TransactionDetailsComponent }];

@NgModule({
  declarations: [TransactionDetailsComponent],
  imports: [CommonModule, RouterModule.forChild(routes), MatCardModule, NgxsModule.forRoot([])],
})
export class TransactionDetailsModule {}
