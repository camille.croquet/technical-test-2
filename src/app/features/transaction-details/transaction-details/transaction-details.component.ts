import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Transaction } from '@models/transaction.model';
import { WalletService } from '@webservices/wallet/wallet.service';
import { Observable } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

@Component({
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionDetailsComponent implements OnInit {
  transaction$: Observable<Transaction>;

  constructor(private readonly walletService: WalletService, private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.transaction$ = this.route.params.pipe(
      filter((params) => !!params.id),
      switchMap((params) => this.walletService.getTransaction(params.id))
    );
  }
}
