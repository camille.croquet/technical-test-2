import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { TransactionModalComponent } from '@commons/transaction-modal/transaction-modal.component';
import { Transaction } from '@models/transaction.model';
import { WalletService } from '@webservices/wallet/wallet.service';

@Component({
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TransactionsComponent implements OnInit, AfterViewInit {
  walletId: string;
  transactions = new MatTableDataSource<Transaction>();
  columnsToDisplay: string[] = ['sender', 'receiver', 'amount'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('transactionDialogTrigger') transactionDialogTrigger: MatButton;

  constructor(
    private readonly walletService: WalletService,
    private readonly router: Router,
    private readonly dialog: MatDialog
  ) {}

  ngAfterViewInit() {
    this.transactions.paginator = this.paginator;
    this.transactions.sort = this.sort;
  }

  ngOnInit() {
    this.walletId = localStorage.getItem('walletId');
    this.setTransactions();
  }

  navigateToTransaction(transactionId) {
    this.router.navigate(['transactions', transactionId]);
  }

  openTransactionDialog() {
    const dialogRef = this.dialog.open(TransactionModalComponent, { data: { walletId: this.walletId }});
    dialogRef.afterClosed().subscribe((moneySent) => {
      if (moneySent) {
        this.setTransactions();
      }
    });
  }

  setTransactions() {
    this.walletService.getTransactions(this.walletId).subscribe((transactions) => {
      this.transactions.data = transactions;
    });
  }
}
