export interface Transaction {
  id: string;
  senderWalletId: string;
  receiverWalletId: string;
  creationDate: string;
  amount: number;
  comment: string;
}
