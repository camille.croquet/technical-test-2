export interface Wallet {
  _id: string;
  balance: number;
  creationDate: string;
  groupId: string;
  lowerLimit: number;
  status: string;
  upperLimit: number;
}
