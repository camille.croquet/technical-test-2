import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { WalletService } from '@webservices/wallet/wallet.service';
import { of, throwError } from 'rxjs';

describe('WalletService', () => {
  let service: WalletService;
  let http: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(WalletService);
    http = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return true if there is a response', (done) => {
    http.head = jest.fn().mockReturnValue(of({}));

    service.checkWalletValidity('WALLET_ID').subscribe((isValid) => {
      expect(isValid).toBeTruthy();
      done();
    });
  });

  it('should return false if there is an error', (done) => {
    http.head = jest.fn().mockReturnValue(throwError({ status: 404 }));

    service.checkWalletValidity('WALLET_ID').subscribe({
      error: (err: HttpErrorResponse) => {
        expect(err.status).toEqual(404);
        done();
      },
    });
  });
});
