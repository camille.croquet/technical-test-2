import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environment';
import { Transaction } from '@models/transaction.model';
import { Wallet } from '@models/wallet.model';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class WalletService {
  baseUrl = `${environment.api.wallet}/v1`;

  constructor(private readonly http: HttpClient) {}

  checkWalletValidity(walletId: string): Observable<Wallet> {
    const request_url = `${this.baseUrl}/wallet/${walletId}`;
    return this.http.get<Wallet>(request_url);
  }

  getTransactions(walletId: string): Observable<Transaction[]> {
    const request_url = `${this.baseUrl}/transactions/${walletId}`;
    return this.http.get<Transaction[]>(request_url);
  }

  getTransaction(transactionId: string): Observable<Transaction> {
    const request_url = `${this.baseUrl}/transaction/${transactionId}`;
    return this.http.get<Transaction>(request_url);
  }

  postTransaction(transaction: Partial<Transaction>): Observable<Transaction> {
    const requestUrl = `${this.baseUrl}/transaction`;
    return this.http.post<Transaction>(requestUrl, transaction);
  }
}
