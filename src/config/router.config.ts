import { ExtraOptions } from '@angular/router';

export const routerConfig: ExtraOptions = {
  initialNavigation: 'enabled',
  onSameUrlNavigation: 'reload',
  paramsInheritanceStrategy: 'always',
  scrollPositionRestoration: 'disabled',
  anchorScrolling: 'enabled',
  relativeLinkResolution: 'legacy',
};
